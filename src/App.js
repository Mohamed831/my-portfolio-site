import React from 'react';
import './App.css';
import { 
   Layout, 
   Header,
   Navigation, 
   Drawer,
   Content,
     } from "react-mdl";
import { Link } from "react-router-dom"
import Main from "./components/main"

function App() {
  return (
    <div className="demo-big-content">
      <Layout>
        <Header
          className="header-color"
          title={
            <span className="name">
              <strong>
                <a href="/">Portfolio</a>
              </strong>
            </span>
          }
          scroll
        >
          <Navigation className="nav">
            <Link to="/aboutme"><span>A Propos de moi</span></Link>
            <Link to="/projects"><span>Projets</span></Link>
            <Link to="/contact"><span>Contact</span></Link>            
          </Navigation>
        </Header>
        <Drawer title="Mohamed">
          <Navigation>
            <Link to="/">Accueil</Link>
            <Link to="/aboutme">A Propos de moi</Link>
            <Link to="/projects">Projets</Link>
            <Link to="/contact">Contact</Link>
          </Navigation>
        </Drawer>
        <Content>
          <div className="page-content" />
          <Main />
        </Content>
        </Layout>
      </div>
  );
}

export default App;
