import React, { Component } from 'react';
import {Grid, Cell} from 'react-mdl';
import FormData from './Form'
import { List, ListItem, ListItemContent } from "react-mdl";

export class Contact extends Component {
    render() {
        return (
          <div className="contact-body">
            <Grid className="contact-grid">
              <Cell col={6}>
                <FormData />
              </Cell>
              <Cell col={6}>
                <h2 className="me">Contacter moi</h2>
                <hr className="separate" />
                <div className="contact-list">
                  <List>
                    <ListItem>
                      <ListItemContent>
                        <i class="fas fa-home"></i>
                        <span>13 Rue Lamartine 94800 Villejuif</span>
                      </ListItemContent>
                    </ListItem>
                    <ListItem>
                      <ListItemContent>
                        <i class="far fa-envelope"></i>
                        <span>mohamed.musa831@gmail.com</span>
                      </ListItemContent>
                    </ListItem>
                    <ListItem>
                      <ListItemContent>
                        <i class="fas fa-phone"></i>
                        <span>+33 6 24 51 25 52</span>
                      </ListItemContent>
                    </ListItem>
                    <ListItem>
                      <ListItemContent>
                        <i class="fab fa-skype"></i>
                        live:mohamed_musa46
                      </ListItemContent>
                    </ListItem>
                  </List>
                </div>
              </Cell>
            </Grid>
          </div>
        );
    }
}

export default Contact
