/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable react/jsx-no-comment-textnodes */
import React, { Component } from "react";
import { Grid, Cell } from "react-mdl";
import Education from "./Education";
import Expérience from "./Experience"
import Skills from "./Skills";

import {
  faArrowRight,
 } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export class Aboutme extends Component {
  render() {
    return (
      <div className="about-body">
        <Grid className="about-grid">
          <Cell col={4}>
            <div className="left-par">
    <p className="paragraph">{" "}Je m’appelle Mohamed MUSA, je suis né le 07/09/1984, j’ai fait mes
              études au SOUDAN en ingénierie pétrolière et j’ai obtenu mon
              bac+5. Ce qui me caractérise le plus c’est mon esprit de
              curiosité. Maintenant en tant que je développeur web, je suis
              pleinement épanoui car j’y retrouve ce qui me plait: le
              développement de fonctionnalités. Aussi, c’est un monde qui répond
              à mon besoin de curiosité et de découvertes car ce monde demande
              de se mettre à jour régulièrement sur les technologies qui évolue
              chaque jour.</p>
              <h3 className="education">Mes Compétences</h3>

              <Skills
                skillTitle={<p id="title">Html/Css</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star-half-alt" />{" "}
                    <i class="far fa-star" />{" "}
                  </div>
                }
              />
              <Skills
                skillTitle={<p id="title">JavaScript</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="far fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                  </div>
                }
              />
              <Skills
                skillTitle={<p id="title">NodeJs</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="far fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                  </div>
                }
              />
              <Skills
                skillTitle={<p id="title">ReactJs</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="far fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                  </div>
                }
              />
              <Skills
                skillTitle={<p id="title">Angular</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                  </div>
                }
              />
              <Skills
                skillTitle={<p id="title">MongoDB</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star-half-alt" /> <i class="far fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                  </div>
                }
              />
              <Skills
                skillTitle={<p id="title">MySQL</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star-half-alt" /> <i class="far fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                  </div>
                }
              />
              <Skills
                skillTitle={<p id="title" >WordPress</p>}
                skillRate={
                  <div className="rate">
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star" />{" "}
                    <i class="fas fa-star" /> <i class="fas fa-star-half-alt" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                    <i class="far fa-star" /> <i class="far fa-star" />{" "}
                  </div>
                }
              />
            </div>
          </Cell>
          <Cell col={8} className="right-side">
            <Grid>
              <Cell col={6}>
                <h3 className="education">Formation</h3>
                <Education
                  period="Mai - Dec 2019"
                  diplomTitle="Développeur web Fullstack JavaScript"
                  schoolName="Simplon | Nanterre"
                  schoolDescription={
                    <div>
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">Créer
                  une base de données.</span>
                      <br /> <FontAwesomeIcon
                        icon={faArrowRight}
                        color="#192a56"
                      />{" "}<span className="jobDesc">
                        Développer les composants d’accès aux données.</span>
                      <br /> <FontAwesomeIcon
                        icon={faArrowRight}
                        color="#192a56"
                      />{" "}<span className="jobDesc">
                        Maquetter une application.</span> <br />
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">
                        Réaliser une interface utilisateur web.</span>

                      <br /> <FontAwesomeIcon
                        icon={faArrowRight}
                        color="#192a56"
                      />{" "}<span className="jobDesc">
                        Développer une interface utilisateur web dynamique.</span>
                      <br /> <FontAwesomeIcon
                        icon={faArrowRight}
                        color="#192a56"
                      />{" "}<span className="jobDesc">
                        Réaliser une interface utilisateur avec une solution de
                  gestion de contenu ou e-commerce.</span>{" "} <a
                        className="link1"
                        href="https://www.simplon.co/"
                        target="_blank"
                      >
                        En savoir plus
                  </a>

                    </div>
                  }
                />
                <Education
                  period="2004 - 2009"
                  diplomTitle="Ingénieur Pétrolier"
                  schoolName="Sudan University Of Science And Technology | Khartoum - Soudan"
                  schoolDescription={
                    <div>
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">Reconnu en France (Maîtrise)</span>{" "}
                      <a
                        className="link1"
                        href="http://www.sustech.edu/"
                        target="_blank"
                      >
                        En savoir plus
                  </a>
                    </div>
                  }
                />
              </Cell>
              <Cell col={6}>
                <h3 className="education">Expérience</h3>
                <Expérience
                  period=" 02 - 27 / Sept 2019"
                  jobTitle="Refonte un site web avec WordPress"
                  entrepriseName="Manufacture Digitale - La Parole aux Sourdes | Paris - France"
                  jobDescription={
                    <div>
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">Changer le désigne du site.</span>
                      <br />
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />
                      <span className="jobDesc"> Mettre à jour des pages.</span>
                      <br />
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">Ajouter des plugin d’événement d'inscription.
                          </span>{" "}<a
                        className="link1"
                        href="https://laparoleauxsourds.fr/"
                        target="_blank"
                      >
                        En savoir plus
                  </a>
                    </div>
                  }
                />
                <Expérience
                  period="2018 - 2019"
                  jobTitle="Technicien de laboratoire de béton"
                  entrepriseName="Société Grand Paris - Ligne 15 Sud | Villejuif - France"
                  jobDescription={
                    <div>
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">Vérifier la formule du béton et le type du ciment .</span>
                      <br />
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">Mesurer la température du béton et le tester.</span>
                      <br />
                      <FontAwesomeIcon icon={faArrowRight} color="#192a56" />{" "}
                      <span className="jobDesc">Mesurer l'étalement du béton.</span>{" "}
                      <a
                        className="link1"
                        href="https://www.societedugrandparis.fr/gpe/ligne/ligne-15-sud"
                        target="_blank"
                      >
                        En savoir plus
                  </a>
                    </div>
                  }
                />
              </Cell>
            </Grid>
          </Cell>
        </Grid>
      </div>
    );
  }
}

export default Aboutme;
