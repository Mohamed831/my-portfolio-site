import React from "react";

function Skills({ skillTitle, skillRate }) {
  return (
    <div className="skill">
      {skillTitle}{" "}{skillRate}
    </div>
  );
}

export default Skills;
