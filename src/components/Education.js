import React from 'react'
import { Grid, Cell } from 'react-mdl';


function Education({
  period,
  diplomTitle,
  schoolName,
  schoolDescription,
  }) {
  return (
    <div>
      <Grid>
        <Cell col={12}>
          <h4 className="diplomTitle">{diplomTitle}</h4>
          <p className="schoolName">
            {schoolName} | {period}
          </p>
          <p>{schoolDescription}</p>
        </Cell>
      </Grid>
    </div>
  );
}

export default Education
