import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Landing from './Landing';
import Aboutme from './Aboutme';
import Projects from './Projects';
import Content  from './Contact';
import Success from './pages/Success'

 const Main = () => (
   <Switch>
     <Route exact path="/" component={Landing} />
     <Route path="/aboutme" component={Aboutme} />
     <Route path="/projects" component={Projects} />
     <Route path="/contact" component={Content} />
     <Route path="/success" component={Success} />
   </Switch>
 );
export default Main;