import React, { Component } from "react";
import { withRouter } from "react-router-dom";

export class Success extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
    };
  }
  goBack = (e) => {
    e.preventDefault();
    this.props.history.push("/");
  };
  render() {
    return (
      <div className="success-body">
        <div class="card-container">
          <p class="text-center">Merci pour votre message</p>
          <a onClick={this.goBack}>
            <i class="fas fa-arrow-left"></i> Retour à l'accueil
          </a>
        </div>
      </div>
    );
  }
}

export default withRouter(Success);
