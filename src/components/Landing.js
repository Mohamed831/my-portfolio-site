import React, { Component } from 'react';
import { Grid, Cell } from 'react-mdl';

export class Landing extends Component {
    render() {
        return (
          <div style={{ width: "100%", margin: "auto" }}>
            <Grid className="landign-grid">
              <Cell col={12}>
                <img
                  src={require("./images/avatar.jpg")}
                  className="avatar-img"
                />
                <div className="banner-text">
                  <h2>Développeur Web Full Stack JavaScript</h2>
                  <hr />
                  <p>
                    {" "}
                    Html | Css | JavaScript | NodeJs | Express | Bootstrap |
                    React | Angular | MySQL | MongoDB
                  </p>
                  <div className="social-links">
                    {/*LinkedIn*/}
                    <a
                      href="https://www.linkedin.com/in/mohamed-musa-0b5582178/"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      <i class="fab fa-linkedin-in"></i>
                    </a>

                    {/*Gitlab*/}
                    <a
                      href="https://gitlab.com/Mohamed831"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      <i class="fab fa-gitlab"></i>
                    </a>
                    {/*Facebook*/}
                    <a
                      href="https://www.facebook.com/mohamed.musa.737"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      <i class="fab fa-facebook-f"></i>
                    </a>
                    {/*Twitter*/}
                    <a
                      href="https://twitter.com/Mohamed_Musa11"
                      rel="noopener noreferrer"
                      target="_blank"
                    >
                      <i class="fab fa-twitter"></i>
                    </a>
                  </div>
                </div>
              </Cell>
            </Grid>
          </div>
        );
    }
}

export default Landing
