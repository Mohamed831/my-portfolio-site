import React from 'react';
import { Grid, Cell } from "react-mdl";
import {  } from "@fortawesome/free-solid-svg-icons";

function Experience({
  period,
  jobTitle,
  entrepriseName,
  jobDescription,
  }) {
  return (
    <div>
      <Grid>
        <Cell col={12}>
          <h4 className="diplomTitle">{jobTitle}</h4>
          <p className="schoolName">
            {entrepriseName} | {period}
          </p>
          <p>{jobDescription}</p>
        </Cell>
      </Grid>
    </div>
  );
}

export default Experience
