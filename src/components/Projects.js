import React, { Component } from 'react'
import { Tabs, Tab, Card, CardText, CardTitle, CardActions,  Button,  Grid, Cell } from 'react-mdl'

export class Projects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeTab: 0
        }
    }
    toggleTabs = () => {
        if (this.state.activeTab === 0) {
            return (
                <Grid>
                    <Cell col={6}>
                        <Card shadow={0} className="card">
                            <CardTitle className="cardTitle"></CardTitle>
                            <CardText>
                                <div className="cardText">
                                C'est une Application d'authentication MERN Stack, 
                                je l'ai réalisé avec Mongodb comme base de donnée, 
                                express, React et Node js. elle contient une page 
                                d'inscription, page de connexion et page de profil
                                qui affiche les données d'utilisateurs et un Button
                                pour se déconnecter.   
                                </div>                                                          
                            </CardText>
                            <CardActions border>
                                <Button
                                 href="https://hidden-wildwood-38649.herokuapp.com/"
                                    target="_blank"
                                 colored>
                                Demo
                                </Button>
                                <Button
                                    href="https://gitlab.com/Mohamed831/mern_stack_authentification/"
                                    target="_blank"
                                     colored>
                                         Code Source
                                </Button>
                            </CardActions>                            
                        </Card>
                    </Cell>
                    <Cell col={6}>
                        <Card shadow={0} className="card1">
                            <CardTitle className="cardTitle1"></CardTitle>
                            <CardText>
                                <div className="cardText">
                                    Cette application de météo je l'ai réalisé 
                                     avec React js, j'ai récupéré les informations
                                     depuis une API extérieure.
                                </div>                                                          
                            </CardText>
                            <CardActions border>
                                <Button
                                 href="https://vibrant-jepsen-ac6168.netlify.app/"
                                    target="_blank"
                                 colored>
                                Demo
                                </Button>
                                <Button
                                    href="https://gitlab.com/Mohamed831/app-m-t-o"
                                    target="_blank"
                                     colored>
                                         Code Source
                                </Button>
                            </CardActions>                            
                        </Card>
                    </Cell>
                </Grid>

            )
        } else if (this.state.activeTab === 1) {
            return (
                <div> Angular Projects</div >
            )
        } else {
            return (
                <div> le rest</div >
            )
        }
    }
    render() {
        return (
            <div className="category-tabs">
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} >
                    <Tab className="tab"><i class="fab fa-react"></i></Tab>
                    <Tab className="tab1"><i class="fab fa-angular"></i></Tab>
                    <Tab className="tab2"><i class="fas fa-code"></i></Tab>
                </Tabs>
                <section className="section">
                    <Grid>
                        <Cell col={12}>
                            <div className="projects-grid">{this.toggleTabs()}</div>
                        </Cell>
                    </Grid>
                </section>
            </div>
        )
    }
}

export default Projects
