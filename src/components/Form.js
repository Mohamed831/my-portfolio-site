import React from "react";


export default function FormData() {
   return (
       <div className="form">
         <h3>Envoyez-moi un message</h3>
         <form action="/success/" method="post">
           <input type="hidden" name="form-name" value="contact" />
           <div class="form-group">
             <input
               class="form-control"
               type="text"
               name="name"
               placeholder="Nom"
               required
             />
           </div>
           <div class="form-group">
             <input
               class="form-control"
               type="email"
               name="email"
               placeholder="Email"
               required
             />
           </div>
           <div class="form-group">
             <textarea
               class="form-control"
               name="message"
               placeholder="Message"
               required
             />
           </div>
           <button class="btn btn-block btn-lg btn-primary" type="submit">
             Send
           </button>
         </form>
       </div>
  );
}
